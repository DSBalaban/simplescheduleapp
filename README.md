https://boiling-torch-7924.firebaseio.com/days

# Schedule App #

A simple demo to showcase achieving two way and three way binding with Angular, while introducing basic concepts and several common ng* directives.

### Dependencies ###

* [Node.js](https://nodejs.org/en/)

### How do I get set up? ###

* Open GIT Bash (or a terminal if GIT is available globally)
* Clone the repository using `git clone https://bitbucket.org/DSBalaban/simplescheduleapp.git`
* Go into the local repo: `cd simplescheduleapp`
* Install server dependencies: `npm install`

### Running the app ###

* Open up CMD or a terminal (or any CLI) and run `node server.js`
* Go to `localhost:8000` 

**Note:** If you have an environment variable named PORT, the server will attempt to use its value.

### Branches ###

** workshop2-* ** are the branches to look out for, workshop2-initial being the first one.

Using `git branch` after cloning will only show the `master` branch.
Use `git branch -a` to see all branches, including remote ones.
Use `git checkout <branch-name>` (where branch name can be one of those listed above) to automatically create a branch tracking the remote one of the same name and switch to it.