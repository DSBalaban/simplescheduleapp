(function() {
    'use strict';

    angular
        .module('schedule')
        .service('seWorkdays', seWorkdays);

    function seWorkdays() {
        this.days = {
            "Monday": [
                {
                    "time": "09:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "11:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "13:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "16:00",
                    "booked": false,
                    "bookedBy": null
                }
            ],
            "Tuesday": [
                {
                    "time": "09:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "11:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "13:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "16:00",
                    "booked": false,
                    "bookedBy": null
                }
            ],
            "Thursday": [
                {
                    "time": "09:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "11:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "13:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "16:00",
                    "booked": false,
                    "bookedBy": null
                }
            ],
            "Friday": [
                {
                    "time": "09:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "11:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "13:00",
                    "booked": false,
                    "bookedBy": null
                },
                {
                    "time": "16:00",
                    "booked": false,
                    "bookedBy": null
                }
            ]
        }
    }
})();