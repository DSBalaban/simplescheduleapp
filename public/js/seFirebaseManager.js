(function() {
    'use strict';

    angular
        .module('schedule')
        .factory('seFirebaseManager', seFirebaseManager);

    function seFirebaseManager($firebaseObject, seWorkdays) {
        var ref = new Firebase('https://boiling-torch-7924.firebaseio.com/days');
        var syncObject = $firebaseObject(ref);

        return {
            bind: function(context, target) {
                syncObject.$bindTo(context, target);
            },
            reset: function() {
                ref.set(seWorkdays.days);
            }
        }
    }
})();