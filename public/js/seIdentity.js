(function() {
    'use strict';

    angular
        .module('schedule')
        .factory('seIdentity', seIdentity);

    function seIdentity() {
        return {
            getIdentity: function() {
                return Math.random().toString(36).substring(2, 7);
            }
        }
    }
})();