(function() {
    'use strict';

    angular
        .module('schedule')
        .controller('seScheduleCtrl', seScheduleCtrl);

    function seScheduleCtrl($scope, seIdentity, seFirebaseManager) {
        var vm = this;

        seFirebaseManager.bind($scope, 'workdays');

        this.identity = {
            id: seIdentity.getIdentity(),
            name: ''
        };

        this.book = function(opening) {
            var me = vm.identity.id;
            var bookedByMe = opening.bookedBy === me;

            if(opening.booked && !bookedByMe) return;

            opening.booked = !opening.booked;
            opening.bookedBy = opening.booked ? me : null;
        };

        this.reset = function() {
            seFirebaseManager.reset();
        };
    }
})();